// std and main are not available for bare metal software
#![no_std]
#![no_main]
//#![deny(unsafe_code)]

extern crate nb;

use cortex_m_rt::{entry, exception, ExceptionFrame};
use panic_semihosting as _;

use cortex_m::peripheral::Peripherals;
use cortex_m_semihosting::hprintln;
use embedded_hal::digital::v2::OutputPin;
use nb::block;
use nb::Error::Other;
use stm32f1xx_hal::{
    pac,
    prelude::*,
    rtc::Rtc,
    serial::{Config, Serial},
};

extern crate core;
use core::fmt::Write;
/*
    LED's:

    Vcc  = ID5

    PC9  = ID4
    PA8  = ID3
    PA9  = ID2 (also TX)
    PA10 = ID1 (also RX)

    chips:

    stm32f103 vet6 y hpacz

    max3232 <- RS232 thing
        J5 (3 pin JST):
            1 7 - DOUT2 - DIN2 - 10 - PA2
            2 8 - RIN2 - ROUT2 - 9 - PA3
            3 GND (right most with 9pinD upwards)
        J6 (?) DB9
            1
            2 TX - 14 - DOUT1 - DIN1 - 11 - PA9
            3 RX - 13 - RIN1 - ROUT1 - 12 - PA10
            4
            5 GND - 10
            6
            7
            8
            9

    vp230 ti 46m a 1 r k <- can transceiver

    sp3485e 1309l c9262 <-- usb or power something?

    atmel1323 45DB161D su <- 16Mbit spi flash

       PA7 <- SI 1 5 SO -> PA6
      PA5 <- SCK 2 6 GND
    PC4 <- reset 3 7 VCC
      PC5  <- CS 4 8 WP -> +VCC via pullup

    ams1117 3.3 q1418 <- 3.3v reg, up to 15v(?)

*/

#[derive(Debug, Eq, PartialEq)]
enum EsqState {
    Looking,
    Bracket,
    Page,
}

fn is_print(c: u8) -> bool {
    if c >= b' ' && c <= b'~' {
        return true;
    }
    false
}

// use `main` as the entry point of this application
#[entry]
fn main() -> ! {
    // get handles to the hardware
    let p = pac::Peripherals::take().unwrap();

    let mut pwr = p.PWR;

    // Take ownership over the raw flash and rcc devices and convert them into the corresponding
    // HAL structs
    let mut flash = p.FLASH.constrain();
    let mut rcc = p.RCC.constrain();

    let mut backup_domain = rcc.bkp.constrain(p.BKP, &mut rcc.apb1, &mut pwr);

    // Freeze the configuration of all the clocks in the system and store the frozen frequencies in
    // `clocks`
    let clocks = rcc
        .cfgr
        .use_hse(8.mhz())
        .sysclk(24.mhz())
        .hclk(24.mhz())
        .freeze(&mut flash.acr);

    // Prepare the alternate function I/O registers
    let mut afio = p.AFIO.constrain(&mut rcc.apb2);

    let pif = Peripherals::take().unwrap();
    let cpuid = pif.CPUID;

    // Prepare the GPIO{A,B,C}
    let mut gpioa = p.GPIOA.split(&mut rcc.apb2);
    //    let mut gpiob = p.GPIOB.split(&mut rcc.apb2);
    let mut gpioc = p.GPIOC.split(&mut rcc.apb2);

    // USART1
    let tx = gpioa.pa9.into_alternate_push_pull(&mut gpioa.crh);
    let rx = gpioa.pa10;

    // LED's
    let mut id3 = gpioa.pa8.into_push_pull_output(&mut gpioa.crh).downgrade();
    let mut id4 = gpioc.pc9.into_push_pull_output(&mut gpioc.crh).downgrade();

    // Set up the usart device. Taks ownership over the USART register and tx/rx pins. The rest of
    // the registers are used to enable and configure the device.
    let serial = Serial::usart1(
        p.USART1,
        (tx, rx),
        &mut afio.mapr,
        Config::default().baudrate(19200.bps()),
        clocks,
        &mut rcc.apb2,
    );

    // let x = serial.listen();

    // Split the serial struct into a receiving and a transmitting part
    let (mut tx, mut rx) = serial.split();

    static ENDL: &str = "\r\n";
    static ERASE: &str = "\x1b[D \x1b[D";

    write!(tx, "Type `help` for help{}", ENDL).unwrap();

    // https://developer.arm.com/documentation/ddi0337/h/system-control/register-descriptions/cpuid-base-register--cpuid

    writeln!(tx, "base: 0x{:08X}\r", cpuid.base.read()).unwrap();
    writeln!(tx, "pfr0: 0x{:08X}\r", cpuid.pfr[0].read()).unwrap();
    writeln!(tx, "pfr1: 0x{:08X}\r", cpuid.pfr[1].read()).unwrap();
    writeln!(tx, "afr: 0x{:08X}\r", cpuid.afr.read()).unwrap();

    let mut rtc = Rtc::rtc(p.RTC, &mut backup_domain);

    writeln!(tx, "time: {}\r", rtc.current_time()).unwrap();

    write!(tx, "backup domain registers:{}", ENDL).unwrap();
    for i in 0..10 {
        write!(
            tx,
            "{}: {:04X}{}",
            i,
            backup_domain.read_data_register_low(i),
            ENDL
        )
        .unwrap();
    }

    // use this instead here:
    // https://github.com/stm32-rs/stm32-device-signature
    // flash size
    let flash_size_ptr: *const u16 = 0x1FFF_F7E0 as *const u16;
    write!(tx, "flash size ptr: {:p}{}", flash_size_ptr, ENDL).unwrap();
    let flash_size: u16 = unsafe { *flash_size_ptr };

    write!(tx, "flash size: {:?}k{}", flash_size, ENDL).unwrap();

    // unique ID registers
    let u_id_base: u32 = 0x1FFF_F7E8;
    let u_id_0_p: *const u16 = u_id_base as *const u16;

    let u_id_stuff: &[u16] = unsafe { core::slice::from_raw_parts(u_id_0_p, 6) };

    for (i, v) in u_id_stuff.iter().enumerate() {
        write!(tx, "u_id {}: {:04X}{}", i, v, ENDL).unwrap();
    }

    // 32 = green
    // 33 = yellowish
    let prompt = "\x1b[33mhello >\x1b[0m ";
    // length of prompt without esc sequences
    const PROMPTLEN: usize = 8;
    write!(tx, "{}", prompt).unwrap();

    let mut i = 0;

    let mut ct = rtc.current_time();
    rtc.set_alarm(ct + 2);

    const BUFLEN: usize = 100;
    let mut buffer: [u8; BUFLEN] = [0; BUFLEN];
    let mut tmpbuffer: [u8; BUFLEN] = [0; BUFLEN];
    let mut bufpos: usize = 0;
    let mut cursor: usize = 0;

    const CR: u8 = b'\r';
    const NL: u8 = b'\n';
    const KEY_SOH: u8 = 1; /* ^A Start of heading, = console interrupt */
    // const KEY_STX: u8 = 2; /* ^B Start of text, maintenance mode on HP console */
    // const KEY_ETX: u8 = 3; /* ^C End of text */
    // const KEY_EOT: u8 = 4; /* ^D End of transmission, not the same as ETB */
    const KEY_ENQ: u8 = 5; /* ^E Enquiry, goes with ACK; old HP flow control */
    const KEY_VT: u8 = 11; /* ^K Vertical tab */
    const ESC: u8 = 0x1b;
    const KEY_BS: u8 = 0x8;
    const KEY_DEL: u8 = 0x7f;
    let mut in_esc_seq = false;

    let mut esq_state: EsqState = EsqState::Looking;

    loop {
        match rtc.wait_alarm() {
            Err(nb::Error::WouldBlock) => {}
            Err(Other(_)) => {
                writeln!(tx, "other?").unwrap();
            }
            Ok(()) => {
                // writeln!(tx, "tick\r").unwrap();
                // races or something???
                ct = rtc.current_time();
                rtc.set_alarm(ct + 2);
                if i == 0 {
                    id3.set_high().unwrap();
                    id4.set_low().unwrap();
                } else {
                    id3.set_low().unwrap();
                    id4.set_high().unwrap();
                }
                i += 1;
                if i > 1 {
                    i = 0;
                }
            }
        }

        match rx.read() {
            Err(nb::Error::WouldBlock) => {}
            Ok(got) => {
                if !in_esc_seq {
                    match got {
                        // backspace or del
                        KEY_BS | KEY_DEL => {
                            if cursor > 0 {
                                write!(tx, "{}", ERASE).unwrap();
                                if cursor != bufpos {
                                    for i in 0..(cursor - 1) {
                                        tmpbuffer[i] = buffer[i];
                                    }
                                    for i in cursor..bufpos {
                                        tmpbuffer[i - 1] = buffer[i];
                                    }
                                    bufpos -= 1;
                                    for i in 0..bufpos {
                                        buffer[i] = tmpbuffer[i];
                                    }
                                    cursor -= 1;
                                    // clear from the cursor onwards
                                    write!(tx, "{}[K", ESC as char).unwrap();
                                    for i in cursor..bufpos {
                                        block!(tx.write(buffer[i])).unwrap();
                                    }
                                    // reset the cursor to after the prompt
                                    write!(
                                        tx,
                                        "\x1b[{}D\x1b[{}C",
                                        BUFLEN + PROMPTLEN + 2,
                                        PROMPTLEN
                                    )
                                    .unwrap();
                                    // now move it to the right place
                                    if cursor > 0 {
                                        write!(tx, "\x1b[{}C", cursor).unwrap();
                                    }
                                } else {
                                    bufpos -= 1;
                                    cursor -= 1;
                                }
                            }
                        }
                        CR => {
                            block!(tx.write(got)).unwrap();
                            block!(tx.write(b'\n')).unwrap();
                            if bufpos > 0 {
                                if &buffer[..] == b"time" {
                                    write!(tx, "time: {}{}", rtc.current_time(), ENDL).unwrap();
                                } else if &buffer[..] == b"reboot" {
                                    write!(tx, "rebooting{}", ENDL).unwrap();
                                    pac::SCB::sys_reset();
                                } else if &buffer[..] == b"help" {
                                    write!(tx, "help:{}", ENDL).unwrap();
                                    write!(tx, "{}", ENDL).unwrap();
                                    write!(tx, "time: print the current time{}", ENDL).unwrap();
                                    write!(tx, "reboot: reset the uc{}", ENDL).unwrap();
                                } else {
                                    for i in 0..bufpos {
                                        block!(tx.write(buffer[i])).unwrap();
                                    }
                                    write!(tx, "{}", ENDL).unwrap();
                                }
                            }
                            bufpos = 0;
                            cursor = 0;
                            write!(tx, "{}", prompt).unwrap();
                        }
                        // printable ascii
                        b' '..=b'~' => {
                            if bufpos < (BUFLEN - 1) {
                                if cursor != bufpos {
                                    for i in 0..cursor {
                                        tmpbuffer[i] = buffer[i];
                                    }
                                    tmpbuffer[cursor] = got;
                                    for i in cursor..bufpos {
                                        tmpbuffer[i + 1] = buffer[i];
                                    }
                                    bufpos += 1;
                                    for i in 0..bufpos {
                                        buffer[i] = tmpbuffer[i];
                                    }
                                    // clear from the cursor onwards
                                    write!(tx, "{}[K", ESC as char).unwrap();
                                    for i in cursor..bufpos {
                                        block!(tx.write(buffer[i])).unwrap();
                                    }
                                    // reset the cursor to after the prompt
                                    write!(
                                        tx,
                                        "\x1b[{}D\x1b[{}C",
                                        BUFLEN + PROMPTLEN + 2,
                                        PROMPTLEN
                                    )
                                    .unwrap();
                                    // now move it to the right place
                                    if cursor > 0 {
                                        write!(tx, "\x1b[{}C", cursor).unwrap();
                                    }
                                    cursor += 1;
                                } else {
                                    buffer[bufpos] = got;
                                    bufpos += 1;
                                    cursor += 1;
                                }
                                block!(tx.write(got)).unwrap();
                            }
                        }
                        ESC => {
                            in_esc_seq = true;
                            esq_state = EsqState::Looking;
                        }
                        KEY_VT => {
                            // delete from cursor to end of line
                            write!(tx, "{}[K", ESC as char).unwrap();
                            bufpos = cursor;
                        }
                        // KEY_SOH ^A - beginning of line
                        // KEY_ENQ ^E - end of line
                        KEY_SOH | KEY_ENQ => {}
                        _ => {
                            write!(tx, "{:02X}", got).unwrap();
                        }
                    }
                } else {
                    // in_esc_seq
                    let mut f: bool = false;
                    match got {
                        b'[' => {
                            esq_state = EsqState::Bracket;
                        }

                        _ => {
                            if esq_state == EsqState::Bracket {
                                match got {
                                    b'A' => {
                                        write!(tx, "UP").unwrap();
                                        f = true;
                                    }
                                    b'B' => {
                                        write!(tx, "DOWN").unwrap();
                                        f = true;
                                    }
                                    // right
                                    b'C' => {
                                        if cursor < bufpos {
                                            write!(tx, "{}[{}C", ESC as char, 1).unwrap();
                                            cursor += 1;
                                        }
                                        f = true;
                                    }
                                    // left
                                    b'D' => {
                                        if cursor > 0 {
                                            write!(tx, "{}[{}D", ESC as char, 1).unwrap();
                                            cursor -= 1;
                                        }
                                        f = true;
                                    }
                                    b'H' => {
                                        write!(tx, "HOME").unwrap();
                                        f = true;
                                    }
                                    b'F' => {
                                        write!(tx, "END").unwrap();
                                        f = true;
                                    }
                                    // page up
                                    b'5' => {
                                        write!(tx, "PGUP").unwrap();
                                        esq_state = EsqState::Page;
                                    }
                                    // page down
                                    b'6' => {
                                        write!(tx, "PGDN").unwrap();
                                        esq_state = EsqState::Page;
                                    }
                                    _ => {
                                        write!(tx, "other ").unwrap();
                                        if is_print(got) {
                                            block!(tx.write(got)).unwrap();
                                        } else {
                                            write!(tx, "{}", got).unwrap();
                                        }
                                        write!(tx, ", {:?}{}", esq_state, ENDL).unwrap();

                                        f = true;
                                    }
                                }
                                if f {
                                    in_esc_seq = false;
                                    esq_state = EsqState::Looking;
                                }
                            } else if esq_state == EsqState::Page {
                                match got {
                                    b'~' => {}
                                    _ => { /* XXX warn here?*/ }
                                }
                                in_esc_seq = false;
                                esq_state = EsqState::Looking;
                            } else {
                                in_esc_seq = false;
                                esq_state = EsqState::Looking;
                            }
                        }
                    }
                }
            }
            Err(Other(_)) => {}
        }
    }
}


#[exception]
fn HardFault(ef: &ExceptionFrame) -> ! {
    panic!("{:#?}", ef);
}

#[exception]
fn DefaultHandler(irqn: i16) {
    panic!("Unhandled exception (IRQn = {})", irqn);
}
